# rails-search-demo
Demo application for geather different search and suggestion techniques implemented in different existing projects.

This demo using free artworks database as seed source. 
Database source: 

[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.1407301.svg)](http://dx.doi.org/10.5281/zenodo.1407301)
 
[GITHUB](https://github.com/MuseumofModernArt/collection)


## Installation
```
rails db:setup
rails db:seed
```

seeds.rb will download, unzip and parse CSV filling the database. 
Notice that seeds.rb will delete all data from models that are seeding before start.