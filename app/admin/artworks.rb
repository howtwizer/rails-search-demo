ActiveAdmin.register Artwork do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :title, :artist, :classification, :department

  form do |f|
    inputs "Details" do
      input :title
    end
  end

end
