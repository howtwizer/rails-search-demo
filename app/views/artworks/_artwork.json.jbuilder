json.extract! artwork, :id, :created_at, :updated_at
json.url artwork_url(artwork, format: :json)
