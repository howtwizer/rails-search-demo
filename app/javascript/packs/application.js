/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb


import 'bootstrap'
import './src/application.scss'

import Vue from 'vue/dist/vue.esm'

import TurbolinksAdapter from 'vue-turbolinks';
Vue.use(TurbolinksAdapter)

import App from '../app.vue'
Vue.component('app', App)

import Autocomplete from './components/shared/vue-autocomplete.vue';
Vue.component('autocomplete', Autocomplete)

import ArtworksSuggester from './components/artworks/suggester.vue'
Vue.component('artworks-suggester', ArtworksSuggester)

import ArtistsSuggester from './components/artists/suggester.vue'
Vue.component('artists-suggester', ArtistsSuggester)

import MultiSuggester from './components/artworks/multi-suggester.vue'
Vue.component('multi-suggester', MultiSuggester)

document.addEventListener('turbolinks:load', () => {
    const app = new Vue({
        el: '[data-behavior="vue"]',
    })

})

console.log('Hello World from Webpacker')