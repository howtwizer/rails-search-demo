Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root to: 'artworks#index'

  resources :departments
  resources :classifications
  resources :artworks
  resources :artists

  namespace :suggests do
    get :artists
    get :artworks
    get :classifications
    get :departments
    get :all_models
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
